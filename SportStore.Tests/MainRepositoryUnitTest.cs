﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportStore.Domain.Abstract;
using SportStore.Domain.Concrete;
using Moq;
using SportStore.Domain.Entities;
using System.Linq;

namespace SportStore.Tests
{
    /// <summary>
    /// Summary description for MainRepositoryUnitTest
    /// </summary>
    [TestClass]
    public class MainRepositoryUnitTest
    {
        [TestMethod]
        public void Can_Compute_Total()
        {
            //Arrange
            Mock<IDiscountHelper> mock = new Mock<IDiscountHelper>();
            mock.Setup(
                    x => x.ApplyDiscount(It.IsAny<decimal>())
                ).Returns<decimal>(
                    x => x
                );

            IMainRepository target = new MainRepository(mock.Object);

            List<Product> products = new List<Product>();
            products.Add(new Product()
            {
                Price = 1
            });
            products.Add(new Product()
            {
                Price = 2
            });
            products.Add(new Product()
            {
                Price = 3
            });

            //Act
            var result = target.ComputeTotal(products);

            //Assert
            Assert.AreEqual(products.Sum(x => x.Price), result);
        }
    }
}
