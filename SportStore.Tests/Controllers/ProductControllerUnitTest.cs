﻿using System.Linq;
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportStore.Domain.Entities;
using Moq;
using SportStore.Domain.Abstract;
using SportStore.WebUI.Controllers;

namespace SportStore.Tests.Controllers
{
    /// <summary>
    /// Summary description for ProductControllerUnitTest
    /// </summary>
    [TestClass]
    public class ProductControllerUnitTest
    {
        private List<Product> products
        {
            get
            {
                return new List<Product>()
                {
                    new Product() {ProductID = 1},
                    new Product() {ProductID = 2},
                    new Product() {ProductID = 3},
                    new Product() {ProductID = 4},
                    new Product() {ProductID = 5}
                };
            }
        }

        [TestMethod]
        public void Can_List_Product_Index()
        {
            //Arrange
            Mock<IMainRepository> mock = new Mock<IMainRepository>();
            mock.Setup(x => x.Products)
                .Returns(products.AsQueryable());

            ProductController target = new ProductController(mock.Object);

            //Act
            var view = target.Index();
            var model = view.Model as List<Product>;
            var viewName = view.ViewName;
            if(string.IsNullOrEmpty(viewName))
            {
                viewName = "Index";
            }
            //Assert
            Assert.IsNotNull(model);
            Assert.AreEqual(products.Count, model.Count);
            Assert.AreEqual("Index", viewName);
        }
    }
}
