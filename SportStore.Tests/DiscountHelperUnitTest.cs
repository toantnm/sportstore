﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportStore.Domain.Abstract;
using SportStore.Domain.Concrete;

namespace SportStore.Tests
{
    /// <summary>
    /// Summary description for DiscountHelperUnitTest
    /// </summary>
    [TestClass]
    public class DiscountHelperUnitTest
    {
        [TestMethod]
        public void Can_Discount()
        {
            //Arrange
            IDiscountHelper target = new DefaultDiscountHelper();
            decimal total = 100;

            //Acc
            var result = target.ApplyDiscount(total);

            //Assert
            Assert.AreEqual(100, result);
        }
    }
}
