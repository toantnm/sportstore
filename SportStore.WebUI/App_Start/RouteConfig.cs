﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SportStore.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //custom route
            routes.MapRoute(
                null,
                "Product/Index/{page}",
                new
                {
                    controller = "Product",
                    action = "Index",
                    category = 0,
                    page = UrlParameter.Optional
                }
                );

            routes.MapRoute(
                null,
                "danh-sach-{category}",
                new
                {
                    controller = "Product",
                    action = "Index",
                    category = UrlParameter.Optional,
                    page = 1
                }
                );

            routes.MapRoute(
                null,
                "danh-sach-{category}/{page}",
                new
                {
                    controller = "Product",
                    action = "Index",
                    category = UrlParameter.Optional,
                    page = UrlParameter.Optional
                }
                );

            //trang chu
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
