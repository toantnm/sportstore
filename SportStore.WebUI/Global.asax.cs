﻿using Ninject;
using Ninject.Web.Common.WebHost;
using SportStore.Domain;
using SportStore.Domain.Abstract;
using SportStore.Domain.Concrete;
using SportStore.Domain.Entities;
using SportStore.WebUI.Binders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SportStore.WebUI
{
    public class MvcApplication : NinjectHttpApplication//System.Web.HttpApplication
    {
        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            ModelBinders.Binders.Add(typeof(Cart), new CartModelBinder());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            RegisterService(kernel);
            return kernel;
        }

        private void RegisterService(StandardKernel kernel)
        {
            kernel.Bind<IMainRepository>().To<MainRepository>();
            kernel.Bind<IDiscountHelper>().To<DefaultDiscountHelper>();
        }
    }
}
