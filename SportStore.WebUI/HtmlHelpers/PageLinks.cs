﻿using SportStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SportStore.WebUI.HtmlHelpers
{
    public static class PageLinks
    {
        public static MvcHtmlString PageLink(this HtmlHelper html, PagingInfo pagingInfo, Func<int, string> pageUrl)
        {
            TagBuilder nav = new TagBuilder("nav");
            TagBuilder ul = new TagBuilder("ul");
            StringBuilder sb = new StringBuilder();

            for(int i = 1; i<= pagingInfo.TotalPages; i++)
            {
                TagBuilder li = new TagBuilder("li");
                TagBuilder a = new TagBuilder("a");
                a.SetInnerText(i.ToString());
                a.Attributes.Add("href", pageUrl(i));

                if(i == pagingInfo.CurrentPage)
                {
                    li.AddCssClass("active");
                }

                li.InnerHtml = a.ToString();
                sb.Append(li);
            }
            ul.InnerHtml = sb.ToString();
            ul.AddCssClass("pagination");
            nav.InnerHtml = ul.ToString();

            return MvcHtmlString.Create(nav.ToString());
        }
    }
}