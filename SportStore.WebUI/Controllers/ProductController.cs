﻿using SportStore.Domain;
using SportStore.Domain.Abstract;
using SportStore.Domain.Concrete;
using SportStore.Domain.Entities;
using SportStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportStore.WebUI.Controllers
{
    public class ProductController : Controller
    {
        //inject repository
        private IMainRepository mainRepository { get; set; }
        public ProductController(IMainRepository mainRepo)
        {
            mainRepository = mainRepo;
        }
        // GET: Product
        public ViewResult Index(int page = 1, int category = 0)
        {
            ProductIndexViewModel model = new ProductIndexViewModel();

            ViewBag.CategoryId = category;

            model.PagingInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = 4,
                TotalItems = mainRepository.Products
                .Where(x => category == 0 || x.CategoryID == category)
                .Count()
            };

            model.Products = mainRepository.Products
                .OrderBy(x => x.ProductID)
                .Where(x => category == 0 || x.CategoryID == category)
                .Skip((page - 1) * model.PagingInfo.ItemsPerPage)
                .Take(model.PagingInfo.ItemsPerPage)
                .ToList();

            return View(model);
        }

        public ViewResult Details(int id)
        {
            var model = mainRepository.Products
                .FirstOrDefault(x => x.ProductID == id);
            if (model == null)
            {
                return View("NotFound");
            }

            return View(model);
        }

        public PartialViewResult GetCategories()
        {
            List<Category> model = mainRepository.Categories.ToList();

            return PartialView(model);
        }
    }
}