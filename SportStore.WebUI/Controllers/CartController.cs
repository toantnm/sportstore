﻿using SportStore.Domain.Abstract;
using SportStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportStore.WebUI.Controllers
{
    public class CartController : Controller
    {
        private IMainRepository mainRepository { get; set; }
        public CartController(IMainRepository mainRepo)
        {
            mainRepository = mainRepo;
        }

        //private Cart GetCart()
        //{
        //    var cart = Session["cart"] as Cart;
        //    if (cart == null)
        //    {
        //        cart = new Cart();
        //        Session["cart"] = cart;
        //    }

        //    return cart;
        //}

        // GET: Cart
        public ActionResult Index(Cart cart, string returnUrl)
        {
            //Cart model = GetCart();
            ViewBag.ReturnUrl = returnUrl;

            return View(cart);
        }

        [HttpPost]
        public ActionResult AddToCart(Cart cart, int productID, string returnUrl)
        {
            var product = mainRepository.Products.FirstOrDefault(x => x.ProductID == productID);
            if (product != null)
            {
                cart.Add(product, 1);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        [HttpPost]
        public ActionResult RemoveFromCart(Cart cart, int productID, string returnUrl)
        {
            var product = mainRepository.Products.FirstOrDefault(x => x.ProductID == productID);
            if (product != null)
            {
                cart.Remove(product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        [HttpPost]
        public JsonResult UpdateQuantityOfItem(Cart cart, int productID, int quantity)
        {
            var product = mainRepository.Products
                .FirstOrDefault(x => x.ProductID == productID);

            if (product != null)
            {
                cart.Update(product, quantity);
                return Json(1);
            }
            else
            {
                return Json(0);
            }
        }

        public ViewResult Checkout()
        {

            return View();
        }
    }
}