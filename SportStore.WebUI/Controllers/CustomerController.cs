﻿using SportStore.Domain.Abstract;
using SportStore.Domain.Entities;
using SportStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportStore.WebUI.Controllers
{
    public class CustomerController : Controller
    {
        //inject repository
        private IMainRepository mainRepository { get; set; }
        public CustomerController(IMainRepository mainRepo)
        {
            mainRepository = mainRepo;
        }

        // GET: Customer
        public ActionResult Index(int page = 1, int country = 0)
        {
            CustomerIndexViewModel model = new CustomerIndexViewModel();

            ViewBag.CountryID = country;

            model.PagingInfo = new PagingInfo()
            {
                CurrentPage = page,
                ItemsPerPage = 4,
                TotalItems = mainRepository.Customers
                .Where(x => country == 0 || x.CountryID == country)
                .Count()
            };

            model.Customer = mainRepository.Customers
                .OrderBy(x => x.CustomerID)
                .Where(x => country == 0 || x.CountryID == country)
                .Skip((page - 1) * model.PagingInfo.ItemsPerPage)
                .Take(model.PagingInfo.ItemsPerPage)
                .ToList();

            return View(model);
        }

        public ViewResult Details(int id)
        {
            var model = mainRepository.Customers
                .FirstOrDefault(x => x.CustomerID == id);
            if (model == null)
            {
                return View("NotFound");
            }

            return View(model);
        }

        public PartialViewResult GetCountries()
        {
            List<Country> model = mainRepository.Countries.ToList();

            return PartialView(model);
        }
    }
}