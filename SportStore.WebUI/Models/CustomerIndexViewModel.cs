﻿using SportStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportStore.WebUI.Models
{
    public class CustomerIndexViewModel
    {
        public List<Customer> Customer { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}