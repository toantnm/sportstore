﻿using SportStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportStore.Domain.Abstract
{
    public interface IMainRepository
    {
        IQueryable<Product> Products { get;}
        IQueryable<Category> Categories { get;}
        IQueryable<Customer> Customers { get; }
        IQueryable<Country> Countries { get; }

        //Insert, Update
        void Save(Product product);
        void Save(Category category);
        void Save(Customer customer);
        void Save(Country country);

        //Delete
        void Delete(Product product);
        void Delete(Category category);
        void Delete(Customer customer);
        void Delete(Country country);

        decimal ComputeTotal(List<Product> product);
    }
}
