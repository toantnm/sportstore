﻿CREATE TABLE [dbo].Categories
(
	[CategoryID] INT NOT NULL PRIMARY KEY,
	CategoryName nvarchar(200)
)
GO
CREATE TABLE Products
(
	ProductID int NOT NULL PRIMARY KEY,
	ProductName nvarchar(200),
	CategoryID int FOREIGN KEY REFERENCES Categories(CategoryID),
	Price decimal (18, 0),
	Amount int,
	ImgUrl nvarchar(200)
)

GO
CREATE TABLE Countries
(
	CountryID int NOT NULL PRIMARY KEY IDENTITY,
	CountryName nvarchar(200)
)
GO
CREATE TABLE Customers
(
	CustomerID int NOT NULL PRIMARY KEY IDENTITY,
	CustomerName nvarchar(200),
	[Address] nvarchar(200),
	[Description] ntext,
	Avatar nvarchar(200),
	CountryID int FOREIGN KEY REFERENCES Countries(CountryID)
)



--DROP TABLE Products
SELECT * FROM Products

INSERT INTO Categories values('1','Test 1')
INSERT INTO Categories values('2','Test 2')
INSERT INTO Categories values('3','Test 3')

INSERT INTO Products values('1', 'San pham 1', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('2', 'San pham 2', '1', 10000, 10, 'https://vn-live-03.slatic.net/p/8/ba-lo-thoi-trang-likita-xanh-1500363005-7587905-e6343636926f0b472f4bef703435a551-catalog_233.jpg')
INSERT INTO Products values('3', 'San pham 3', '1', 10000, 10, 'https://vn-live-01.slatic.net/p/8/tui-du-lich-ba-lo-da-gia-re-balo-nho-cho-nam-balo-thiet-ke-tre-trung-hien-dai-thoi-trang-mau-moi-nhathot-nhat-nam-1511967669-67584422-1d52b234c4f3f9bd0ab5766375608e63-catalog_233.jpg')
INSERT INTO Products values('4', 'San pham 4', '1', 10000, 10, 'https://vn-live-02.slatic.net/p/8/ba-lo-kieu-phong-cach-tre-trung-1500526912-8767938-857bb1d7eb07d3ad4bd04a7e3510644a-catalog_233.jpg')
INSERT INTO Products values('5', 'San pham 5', '1', 10000, 10, 'https://vn-live-03.slatic.net/p/8/ba-lo-campus-praza-bl140-den-1509892459-42605802-7b2c691a27fc98ef201dcef6bdcd0ab7-catalog_233.jpg')
INSERT INTO Products values('6', 'San pham 6', '1', 10000, 10, 'https://vn-live-01.slatic.net/p/8/ba-lo-laptop-cao-cap-mau-den-phoi-do-1513174138-9477597-dbac70b5525b1a35c9584386a23df272-catalog_233.jpg')
INSERT INTO Products values('7', 'San pham 7', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('8', 'San pham 8', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('9', 'San pham 9', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('10', 'San pham 10', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('11', 'San pham 11', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('12', 'San pham 12', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('13', 'San pham 13', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('14', 'San pham 14', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('15', 'San pham 15', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('16', 'San pham 16', '1', 10000, 10, 'https://vn-live-01.slatic.net/p/8/ba-lo-laptop-cao-cap-mau-den-phoi-do-1513174138-9477597-dbac70b5525b1a35c9584386a23df272-catalog_233.jpg')
INSERT INTO Products values('17', 'San pham 17', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('18', 'San pham 18', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('19', 'San pham 19', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('20', 'San pham 20', '1', 10000, 10, 'sanpham1.jpg')
INSERT INTO Products values('21', 'San pham 21', '1', 10000, 10, 'sanpham1.jpg')
