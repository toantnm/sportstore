﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportStore.Domain.Entities
{
    public class Customer
    {        
        [Key]
        public int CustomerID { get; set; }
        
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Avatar { get; set; }
        public int CountryID { get; set; }

        public virtual Country Country { get; set; }
    }
}
