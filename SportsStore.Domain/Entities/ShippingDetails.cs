﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportStore.Domain.Entities
{
    public class ShippingDetails
    {
        [Required(ErrorMessage = "Vui long nhap ho")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Vui long nhap ten")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Vui long nhap email")]
        [EmailAddress(ErrorMessage = "Khong dung dinh dang")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vui long nhap SDT")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Vui long nhap dia chi")]
        public string Address { get; set; }

        public bool GiftWrap { get; set; }
        public string Note { get; set; }
    }
}
