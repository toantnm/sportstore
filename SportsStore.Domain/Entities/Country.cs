﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportStore.Domain.Entities
{
    public class Country
    {
        [Key]
        public int CountryID { get; set; }

        public string CountryName { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }

        public Country()
        {
            Customers = new HashSet<Customer>();
        }
    }
}
