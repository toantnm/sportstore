﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportStore.Domain.Entities
{
    public class Cart
    {
        public List<CartLine> LineCollection { get; private set; }

        public Cart()
        {
            LineCollection = new List<CartLine>();
        }

        //add
        public void Add(Product product, int quantity)
        {
            var line = LineCollection
                .FirstOrDefault(x => x.Product.ProductID == product.ProductID);
            if (line == null)
            {
                LineCollection.Add(
                    new CartLine()
                    {
                        Product = product,
                        Quantity = quantity
                    });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        //remove
        public void Remove(Product product)
        {
            var line = LineCollection.FirstOrDefault(x => x.Product.ProductID == product.ProductID);
            if(line != null)
            {
                LineCollection.Remove(line);
            }
        }

        //clear
        public void Clear()
        {
            LineCollection.Clear();
        }

        //Compute total
        public decimal ComputeTotal()
        {
            return LineCollection.Sum(x => x.Quantity * x.Product.Price);
        }

        //update
        public void Update(Product product, int quantity)
        {
            var line = LineCollection.FirstOrDefault(x => x.Product.ProductID == product.ProductID);
            if(line != null)
            {
                line.Quantity = quantity;
            }
        }
    }
}
