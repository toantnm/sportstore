﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportStore.Domain.Entities
{
    public class Product
    {
        [Key]
        public int ProductID { get; set; }

        public int CategoryID { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public string ImgUrl { get; set; }
        public string Description { get; set; }
        
        public virtual Category Category { get; set; }
    }
}
