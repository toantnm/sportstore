﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportStore.Domain.Abstract;
using SportStore.Domain.Entities;

namespace SportStore.Domain.Concrete
{
    public class MainRepository : IMainRepository
    {
        private SportsStoreEntities context = new SportsStoreEntities();

        private IDiscountHelper discountHelper { get; set; }
        public MainRepository(IDiscountHelper discounter)
        {
            discountHelper = discounter;
        }

        public IQueryable<Product> Products
        {
            get
            {
                return context.Products;
            }
        }
        public IQueryable<Category> Categories
        {
            get
            {
                return context.Categories;
            }
        }

        public IQueryable<Country> Countries
        {
            get
            {
                return context.Countries;
            }
        }

        public IQueryable<Customer> Customers
        {
            get
            {
                return context.Customers;
            }
        }

        public decimal ComputeTotal(List<Product> product)
        {
            return discountHelper.ApplyDiscount(product.Sum(x => x.Price));
        }

        public void Delete(Product product)
        {
            var tmp = context.Products.FirstOrDefault(x => x.ProductID == product.ProductID);
            if(tmp != null)
            {
                context.Products.Remove(tmp);
                context.SaveChanges();
            }
        }

        public void Delete(Category category)
        {
            var tmp = context.Categories.FirstOrDefault(x => x.CategoryID == category.CategoryID);
            if (tmp != null)
            {
                context.Categories.Remove(tmp);
                context.SaveChanges();
            }
        }

        public void Delete(Customer customer)
        {
            var tmp = context.Customers.FirstOrDefault(x => x.CustomerID == customer.CustomerID);
            if (tmp != null)
            {
                context.Customers.Remove(tmp);
                context.SaveChanges();
            }
        }

        public void Delete(Country country)
        {
            var tmp = context.Countries.FirstOrDefault(x => x.CountryID == country.CountryID);
            if (tmp != null)
            {
                context.Countries.Remove(tmp);
                context.SaveChanges();
            }
        }

        public void Save(Product product)
        {
            if (product.ProductID == 0)
            {
                context.Products.Add(product);
            }
            else
            {
                var tmp = context.Products.FirstOrDefault(x => x.ProductID == product.ProductID);
                if (tmp != null)
                {
                    tmp.ProductName = product.ProductName;
                    tmp.CategoryID = product.CategoryID;
                    tmp.ImgUrl = product.ImgUrl;
                    tmp.Price = product.Price;
                    tmp.Amount = product.Amount;
                }
                context.SaveChanges();
            }
        }

        public void Save(Category category)
        {
            if(category.CategoryID ==0)
            {
                context.Categories.Add(category);
            }
            else
            {
                var tmp = context.Categories.FirstOrDefault(x => x.CategoryID == category.CategoryID);
                if(tmp!= null)
                {
                    tmp.CategoryName = category.CategoryName;
                }
                context.SaveChanges();
            }
        }

        public void Save(Customer customer)
        {
            if (customer.CustomerID == 0)
            {
                context.Customers.Add(customer);
            }
            else
            {
                var tmp = context.Customers.FirstOrDefault(x => x.CustomerID == customer.CustomerID);
                if (tmp != null)
                {
                    tmp.CustomerID = customer.CustomerID;
                    tmp.CountryID = customer.CountryID;
                    tmp.CustomerName = customer.CustomerName;
                    tmp.Avatar = customer.Avatar;
                    tmp.Address = customer.Address;
                    tmp.Description = customer.Description;
                }
                context.SaveChanges();
            }
        }

        public void Save(Country country)
        {
            if (country.CountryID == 0)
            {
                context.Countries.Add(country);
            }
            else
            {
                var tmp = context.Countries.FirstOrDefault(x => x.CountryID == country.CountryID);
                if (tmp != null)
                {
                    tmp.CountryName = country.CountryName;
                }
                context.SaveChanges();
            }
        }
    }
}
